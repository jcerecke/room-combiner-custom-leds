# Room Combiner Custom LEDs

## Description

Set your own colours for the Room Combiner!

## Donate

This plugin is provided as open source software for you to use and modify. If you use this for commercial purpose, please consider donating in order to help with development costs.

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZYF826FYE4R8E&source=url)

## Usage

1. Name your Room Combine component so it can be discovered by this plugin.
2. Set the number of Rooms properties of both the Room Combine component and this plugin to be the same.
3. If you would like to have custom Off colors, enable this in the plugin properties (off by default).
4. Run/Emulate your design.
5. Select your Room Combiner Name that this plugin should target.
6. Type in your colors for each Room's LED. Color metadata is in the form of a string. Q-SYS uses the built in .NET string->color converter which allows for strings in the form "#RGB", "#RRGGBB", CSS Color Names, and HSV values using the format "!HHSSVV".