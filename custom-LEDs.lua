PluginInfo =
{
  Name = "Room Combiner Add-ons~Custom LEDs",
  Version = "1.0.0",
  Id = "1ad2ee4f-f857-4aaa-8b4b-5af20df5d40a",
  Description = "Set your own colours for the Room Combiner!",
  Author = "Joshua Cerecke - Ceretech Audiovisual Solutions",
  URL = "https://bitbucket.org/jcerecke/room-combiner-custom-leds/"

}

function GetPrettyName(props)
	return "Custom LEDs for Room Combiner V" .. PluginInfo.Version
end

function GetColor(props)
    return { 191, 226, 249 } --Same colour as Room Combiner component
  end

local Colors = {
    White = {255, 255, 255},
    Black = {0, 0, 0},
    Transparent = "FFFFFFFF",
}

function GetProperties()
	props = {
		{
			Name = "Rooms",
			Type = "integer",
			Value = 8,
			Min = 2,
			Max = 256
        },
        {
            Name = "Custom Off Colors",
            Type = "boolean",
            Value = "false"
        }
	}
	return props
end

function GetControls(props)
	ctls = {
		{
			Name = "RoomCombinerTarget",
			ControlType = "Text",
			TextBoxStyle = "ComboBox",
			Count = 1,
			PinStyle = "Input",
			UserPin = true
        },
        {
			Name = "Status",
			ControlType = "Indicator",
			IndicatorType = "Status",
			Count = 1,
			PinStyle = "Output",
			UserPin = true
		},
		{
			Name = "Colors",
			ControlType = "Text",
			TextBoxStyle = "Normal",
			Count = props["Rooms"].Value,
			PinStyle = "Input",
			UserPin = true
        },
        {
			Name = "LEDs",
			ControlType = "Indicator",
			IndicatorType = "Led",
			Count = props["Rooms"].Value,
			PinStyle = "Output",
            UserPin = true
        }
    }
    if props["Custom Off Colors"].Value then
        table.insert(ctls, {
            Name = "OffColors",
            ControlType = "Text",
            TextBoxStyle = "Normal",
            Count = props.Rooms.Value,
            PinStyle = "Input",
            UserPin = true
            }
        )
    end

	return ctls
end

-- This function allows you to layout pages in your plugin.
function GetControlLayout(props)
	local roomQty = props["Rooms"].Value
	-- layout holds representaiton of Controls
	local layout = {}
	-- graphics holds aesthetic & design items
	local graphics = {}
	-- x,y allows an easy method of knowing where you are relative to the section being designed

    table.insert(
	    graphics, {
            Type = "Text",
            Text = "Room Combiner Target",
            FontSize = 11,
            FontStyle = "Regular",
            HTextAlign = "Right",
            Color = {51, 51, 51, 255},
            Fill = {0, 0, 0, 0},
            Position = {20, 20},
            Size = {136, 20},
            ZOrder = 1
        }
    )
    layout["RoomCombinerTarget"] = {
		PrettyName = "Room Combiner Target",
		Style = "ComboBox",
		Position = {166, 20},
		Size = {288, 20},
		FontSize = 11
    }
	table.insert(
        graphics, {
            Type = "Text",
            Text = "Status",
            FontSize = 11,
            FontStyle = "Regular",
            HTextAlign = "Right",
            Color = {51, 51, 51, 255},
            Fill = {0, 0, 0, 0},
            Position = {20, 44},
            Size = {136, 48},
            ZOrder = 1
        }
	)
	layout["Status"] = {
		PrettyName = "Status",
		Style = "Indicator",
		IndicatorStyle = "Status",
		Position = {166, 44},
		Size = {288, 48},
		FontSize = 11,
		ZOrder = 16
	}
    local x, y = 162, 106
    local sizeY = props["Custom Off Colors"].Value and 88 or 72
    table.insert(
        graphics, {
            Type = "GroupBox",
            Text = "Rooms",
            FontSize = 11,
            FontStyle = "Regular",
            HTextAlign = "Left",
            StrokeWidth = 1,
            CornerRadius = 8,
            Color = {51, 51, 51, 255},
            Fill = {0, 0, 0, 0},
            Position = {x, y},
            Size = {roomQty * 36 + 8, sizeY},
            ZOrder = 1
        }
    )
    x, y = 20, y + 20
    table.insert(
        graphics, {
        Type = "Text",
        Text = "Custom LED",
        FontSize = 11,
        FontStyle = "Regular",
		HTextAlign = "Right",
		Color = {51, 51, 51, 255},
		Fill = {0, 0, 0, 0},
		Position = {x, y + 16},
		Size = {136, 16},
		ZOrder = 1
        }
    )
    table.insert(
        graphics, {
        Type = "Text",
        Text = "Color",
        FontSize = 11,
        FontStyle = "Regular",
		HTextAlign = "Right",
		Color = {51, 51, 51, 255},
		Fill = {0, 0, 0, 0},
		Position = {x, y + 32},
		Size = {136, 16},
		ZOrder = 1
        }
    )
    if props["Custom Off Colors"].Value then
        table.insert(
            graphics, {
            Type = "Text",
            Text = "Off Color",
            FontSize = 11,
            FontStyle = "Regular",
            HTextAlign = "Right",
            Color = {51, 51, 51, 255},
            Fill = {0, 0, 0, 0},
            Position = {x, y + 48},
            Size = {136, 16},
            ZOrder = 1
            }
        )
    end
    x = 166
	for i = 1, roomQty do
		table.insert(
            graphics,{
                Type = "Text",
                Text = tostring(i),
                FontSize = 11,
                HTextAlign = "Center",
                Color = Colors.Black,
                Position = {x, y},
                Size = {36, 16}
            }
        )
        layout["LEDs " .. tostring(i)] = {
            PrettyName = "LEDs~Room " .. tostring(i),
            Style = "Led",
            Position = {x + 10, y + 16},
            Size = {16, 16}
        }
		layout["Colors " .. tostring(i)] = {
            PrettyName = "Colors~Room " .. tostring(i),
			Style = "Text",
			TextBoxStyle = "Normal",
			Position = {x, y + 32},
			Size = {36, 16},
			HTextAlign = "Center",
			FontSize = 9,
			Color = Colors.White,
			StrokeWidth = 1
        }
        if props["Custom Off Colors"].Value then
            layout["OffColors "..tostring(i)] = {
                PrettyName = "Off Colors~Room " .. tostring(i),
                Style = "Text",
                TextBoxStyle = "Normal",
                Position = {x, y + 48},
                Size = {36, 16},
                HTextAlign = "Center",
                FontSize = 9,
                Color = Colors.White,
                StrokeWidth = 1
            }
        end
		x = x + 36
    end

	return layout, graphics
end

if Controls then
	-- Global Variables
    RoomCombinerNames = {}
    for i, v in pairs(Component.GetComponents()) do -- Search for all Room Combiner Components
		if v.Type == "room_combiner" then
			table.insert(RoomCombinerNames, v.Name)
		end
    end
    Controls.RoomCombinerTarget.Choices = RoomCombinerNames

    -- Control & Property Aliases
    CustomLEDs = Controls.LEDs
    CustomColors = Controls.Colors
    OffColorsMode = Properties["Custom Off Colors"].Value
    RoomQuantity = Properties.Rooms.Value

    if OffColorsMode then
        CustomOffColors = Controls.OffColors
    end
    RoomCombinerLEDs = {} -- gets populated in Initialize()
    
	-- Functions
    function UpdateStatus(val, msg) -- Updates status, "val" can be passed a number or string.
		if type(val) == "string" then
			val = val == "OK" and 0 or val
			val = val == "Compromised" and 1 or val
			val = val == "Fault" and 2 or val
			val = val == "Not Present" and 3 or val
			val = val == "Missing" and 4 or val
			val = val == "Initializing" and 5 or val
			if type(val) ~= "number" then
				error("UpdateStatus val is incorrectly formatted: Expecting integer 0-5 or valid status string, Received: "..val)
			end
		end
		Controls.Status.Value = val
		if msg ~= nil then
			Controls.Status.String = msg
		end
    end
    
    function IsComponentValid(name)  -- returns true or false if a named component is found.
		return #Component.GetControls(name) ~= 0 and true or false
    end
       
    -- updates the table that stores the groups of rooms
    function WallHasChanged()
        if Controls.Status.String == "OK" then
            local groupColor = nil
            local groupLeader = nil
            for room = 1, RoomQuantity do
                if RoomCombinerLEDs[room].Color ~= groupColor then -- found a new group
                    groupColor = RoomCombinerLEDs[room].Color
                    groupLeader = room
                end
                CustomLEDs[room].Boolean = RoomCombinerLEDs[room].Boolean
                if OffColorsMode and not RoomCombinerLEDs[room].Boolean then
                    CustomLEDs[room].Color = CustomOffColors[groupLeader].String
                else
                    CustomLEDs[room].Color = CustomColors[groupLeader].String
                end

            end
        end
        
    end

    function Initialize()
		UpdateStatus("Initializing")
		
		if IsComponentValid(Controls.RoomCombinerTarget.String) then
			TargetRoomCombiner = Component.New(Controls.RoomCombinerTarget.String)
            local highestRoom = 0

			for name, control in pairs(TargetRoomCombiner) do -- Initialise room combiner controls
				if name:find("output%.%d+%.combined") then
                    local room = tonumber(name:match("output%.(%d+)%.combined"))
                    highestRoom = room > highestRoom and room or highestRoom

                    if room <= RoomQuantity then
                        RoomCombinerLEDs[room] = control
                    else
                        UpdateStatus("Fault", "Room quantity mismatch - Room Combiner: "..tostring(highestRoom)..", This Plugin: "..tostring(RoomQuantity))
                        break
                    end
					
				elseif name:find("wall%.%d+%.open") then -- Add each wall's open button into an array and assign eventhandler to watch it
					control.EventHandler = function() 
						Timer.CallAfter(WallHasChanged, 0.01) -- needs a snall delay to wait for the Room Combiner LEDs to update.
					end
				end
			end

            if RoomQuantity ~= highestRoom then
                UpdateStatus("Fault", "Room quantity mismatch - Room Combiner: "..tostring(highestRoom)..", This Plugin: "..tostring(RoomQuantity))
            end

			if Controls.Status.String == "Initializing" then -- Finished initialising with no faults, update status to OK.
                UpdateStatus("OK")
			end

		else
            UpdateStatus("Fault", "Select a named Room Combiner Component as your target.")
		end
        WallHasChanged()
	end

    -- EventHandlers
    for i = 1, RoomQuantity do
        CustomColors[i].EventHandler = WallHasChanged
        if OffColorsMode then
            CustomOffColors[i].EventHandler = WallHasChanged
        end
    end
    Controls.RoomCombinerTarget.EventHandler = Initialize

    -- Run at Start
    Initialize()

end